<?php

// include file koneksi database

$cekmutasi = array(
    "api_signature" => "y897jC3GzaXKy4XTxrs9v0RcD6qZoOqw"
);

$incomingApiSignature = isset($_SERVER['HTTP_API_SIGNATURE']) ? $_SERVER['HTTP_API_SIGNATURE'] : '';

// validasi API Signature
if( !hash_equals($cekmutasi['api_signature'], $incomingApiSignature) ) {
    exit("Invalid Signature");
}

$post = file_get_contents("php://input");
$json = json_decode($post);

if( json_last_error() !== JSON_ERROR_NONE ) {
    exit("Invalid JSON");
}

if( $json->action == "payment_report" )
{
    foreach( $json->content->data as $data )
    {
        # Waktu transaksi dalam format unix timestamp
        $time = $data->unix_timestamp;

        # Tipe transaksi : credit / debit
        $type = $data->type;

        # Jumlah (2 desimal) : 50000.00
        $amount = $data->amount;

        # Berita transfer
        $description = $data->description;

        # Saldo rekening (2 desimal) : 1500000.00
        $balance = $data->balance;
        
        if( $type == "credit" ) // dana masuk
        {
            echo "success";
        }
    }
}
?>